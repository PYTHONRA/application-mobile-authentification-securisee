import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Authenticate from "./components/auth";
import Login from "./components/login";
import Register from "./components/register";
import Home from "./components/home";

const Stack = createNativeStackNavigator();
const App = () => {
  return (
    <>
      <Authenticate />
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen name="Login" component={Login} />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default App;
