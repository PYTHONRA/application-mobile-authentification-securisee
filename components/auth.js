import React, { useState } from "react";
import instance from "../utils/api";
import { View, Button } from "react-native";
import { getLocalStorage } from "../utils/localStorage";

export default function Authenticate() {
  //states => vérifie si l'utilisateur est auth = valide le token user
  const [isLoading, setIsLoading] = React.useState(false);
  const [error, setError] = useState(null);
  const [fieldError, setFieldError] = useState(null);
  const [redirect, setRedirect] = useState(false);

  const handleSubmit = async (_e) => {
    setError(null);
    setFieldError(null);
    setIsLoading(false);

    try {
      const xsrfToken = getLocalStorage();

      instance.defaults.headers.common["x-xsrf-token"] = xsrfToken.xsrfToken;
      instance.defaults.headers.common["Authorization"] =
        "Bearer " + xsrfToken.accessToken;

      // On effectue la requête
      const result = await instance.get("/auth/me/");
      console.log(result);

      if (result.status === 200) {
        console.log(result);
        // setRedirect(true)
      }
    } catch (err) {
      setError(err.response);
      console.log("erreur : " + error);
    }
  };

  return (
    <View>
      <Button
        title="Authenticate me"
        onPress={() => {
          handleSubmit();
        }}
      />
    </View>
  );
}
