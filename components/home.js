import * as React from "react";
import { Button, View, TouchableHighlight } from "react-native";

function Home({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <TouchableHighlight
        style={{
          height: 40,
          width: 160,
          borderRadius: 10,
          marginLeft: 50,
          marginRight: 50,
          marginTop: 20,
        }}
      >
        <Button
          title="Auth me"
          color="black"
          onPress={() => navigation.navigate("Authenticate")}
        />
      </TouchableHighlight>
      <TouchableHighlight
        style={{
          height: 40,
          width: 160,
          borderRadius: 10,
          marginLeft: 50,
          marginRight: 50,
          marginTop: 20,
        }}
      >
        <Button
          title="Register"
          color="black"
          onPress={() => navigation.navigate("Register")}
        />
      </TouchableHighlight>

      <TouchableHighlight
        style={{
          height: 40,
          width: 160,
          borderRadius: 10,
          marginLeft: 50,
          marginRight: 50,
          marginTop: 20,
        }}
      >
        <Button
          title="Login"
          color="black"
          onPress={() => navigation.navigate("Login")}
        />
      </TouchableHighlight>
    </View>
  );
}

export default Home;
