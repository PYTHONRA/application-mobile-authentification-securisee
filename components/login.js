import React, { useState } from "react";
import { View, Button, TextInput, TouchableHighlight } from "react-native";
import { setLocalStorage } from "../utils/localStorage";
import api from "../utils/api";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [redirect, setRedirect] = useState(false);
  const [error, setError] = useState("");

  if (redirect) {
    return (to = "/Home");
  }

  const handleSubmit = async (_e) => {
    setError("");
    if (!password || !email) {
      setError("Petite erreur ici");
      return null;
    }
    //on voit si on est bien dans la bdd

    try {
      const result = await api.post("/auth/login", {
        email,
        password,
      });
      console.log(result);

      if (result.status === 201) {
        setRedirect(true);
        setLocalStorage(result.data);
        console.log("it's okey !");
      }
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <TextInput
        style={styles.textinput}
        value={email}
        placeholder="email"
        onChangeText={(email) => setEmail(email)}
      />
      <TextInput
        style={styles.textinput}
        value={password}
        placeholder="Password"
        secureTextEntry={true}
        onChangeText={(password) => setPassword(password)}
      />
      <TouchableHighlight
        style={{
          height: 40,
          width: 160,
          borderRadius: 10,
          marginLeft: 50,
          marginRight: 50,
          marginTop: 20,
        }}
      >
        <Button title="Login" color="black" onPress={() => handleSubmit()} />
      </TouchableHighlight>
    </View>
  );
}

const styles = {
  textinput: {
    height: 40,
    width: 160,
    borderRadius: 10,
    marginLeft: 50,
    marginRight: 50,
    marginTop: 20,
  },
};
