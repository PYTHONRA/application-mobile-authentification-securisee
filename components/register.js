import React, { useState } from "react";
import { View, Button, TextInput, TouchableHighlight } from "react-native";
import api from "../utils/api";

export default function Register() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [firstname, setFirstname] = useState("");
  const [username, setUsername] = useState("");
  const [lastname, setLastname] = useState("");
  const [redirect, setRedirect] = useState(false);
  const [error, setError] = useState("");

  /*if (redirect) {
    return (to = "/login");
  }*/
  const handleSubmit = async (_e) => {
    setError("");
    if (!firstname || !lastname || !password || !username || !email) {
      setError("Petite erreur ici");
      return null;
    }
    if (password.length > 15 || password.length < 6) {
      setError("il manque quelque chose !");
      return null;
    }

    try {
      const result = await api.post("/users/", {
        firstname,
        lastname,
        username,
        email,
        password,
      });
      console.log(result);

      if (result.status === 201) {
        setRedirect(true);
      }
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <TextInput
        style={{
          height: 40,
          width: 160,
          borderRadius: 10,
          marginLeft: 50,
          marginRight: 50,
          marginTop: 20,
        }}
        value={firstname}
        placeholder="Firstname"
        onChangeText={(firstname) => setFirstname(firstname)}
      />
      <TextInput
        style={{
          height: 40,
          width: 160,
          borderRadius: 10,
          marginLeft: 50,
          marginRight: 50,
          marginTop: 20,
        }}
        value={lastname}
        placeholder="Lastname"
        onChangeText={(lastname) => setLastname(lastname)}
      />
      <TextInput
        style={{
          height: 40,
          width: 160,
          borderRadius: 10,
          marginLeft: 50,
          marginRight: 50,
          marginTop: 20,
        }}
        placeholder="Password"
        value={password}
        secureTextEntry={true}
        onChangeText={(password) => setPassword(password)}
      />
      <TextInput
        style={{
          height: 40,
          width: 160,
          borderRadius: 10,
          marginLeft: 50,
          marginRight: 50,
          marginTop: 20,
        }}
        value={username}
        placeholder="Username"
        onChangeText={(username) => setUsername(username)}
      />
      <TextInput
        style={{
          height: 40,
          width: 160,
          borderRadius: 10,
          marginLeft: 50,
          marginRight: 50,
          marginTop: 20,
        }}
        value={email}
        placeholder="Email"
        onChangeText={(email) => setEmail(email)}
      />

      <TouchableHighlight
        style={{
          height: 40,
          width: 160,
          borderRadius: 10,
          marginLeft: 50,
          marginRight: 50,
          marginTop: 20,
        }}
      >
        <Button title="Register" color="black" onPress={() => handleSubmit()} />
      </TouchableHighlight>
    </View>
  );
}
